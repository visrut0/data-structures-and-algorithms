def max_sum_no_adjacent(array : list):
    # if length is 1 return first element
    if len(array) == 1:
        return array[0]

    adjSum = array[:]
    adjSum[1] = max(array[0],array[1])

    for i in range(2,len(array)):
        adjSum[i] = max(adjSum[i-1],adjSum[i-2] + array[i])

    return adjSum[-1]

print(max_sum_no_adjacent([7,10,12,7,9,14]))
print(max_sum_no_adjacent([7,10]))
print(max_sum_no_adjacent([9]))
