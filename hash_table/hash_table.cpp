#include <iostream>

template <typename Key,typename Value>
class Pair {
    public:
        Key key;
        Value value;
        Pair<Key,Value>* next;

        Pair(Key key, Value value)
            : key(key) , value(value) , next(nullptr) {};

        ~Pair() {
            delete next;
        }
};

template <typename Key,typename Value>
class Map {
    private:
        Pair<Key,Value>** buckets;
        long count = 0;
        long numBuckets = 0;

        long getHash(Key key) {
            typeid(Key).name();
        }

    public:
        Map()
            : count(0) , numBuckets(500), buckets(new Pair<Key,Value>*[numBuckets]) {
                for(int i=0;i<numBuckets;i++) {
                    buckets[i] = nullptr;
                }
            }

        ~Map() {
            for(int i=0;i<numBuckets;i++) {
                delete buckets[i];
            }
            delete []buckets;
        }

        int size() const {
            return count;
        }

        Value getValue(Key key) {

        }

        void insert(Key &key,Value &value) {

        }

        Value remove(Key key) {

        }
};

int main() {

    return 0;
}
