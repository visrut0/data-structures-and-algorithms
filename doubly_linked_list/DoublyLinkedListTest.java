interface IDoublyLinkedList<T> {
    void insertAtHead(T data);
    void insertAtTail(T data);
    T removeAtHead();
    T removeAtTail();
    void printFromHead();
    void printFromTail();
}

class Node<T> {
    T data;
    Node<T> next;
    Node<T> previous;

    Node(T data) {
        this.data = data;
        this.next = null;
        this.previous = null;
    }

    @Override
    public String toString() {
        return "[ " + this.data + " ]";
    }
}

class DoublyLinkedList<T> implements IDoublyLinkedList<T>{
    Node<T> head;
    Node<T> tail;

    DoublyLinkedList() {
        this.head = null;
        this.tail = null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void insertAtHead(T data) {
        if(this.head == null) {
            this.head = this.tail = new Node(data);
        } else {
            Node<T> newNode = new Node(data);
            newNode.next = this.head;
            this.head.previous = newNode;
            this.head = newNode;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void insertAtTail(T data) {
        if(this.tail == null) {
            this.head = this.tail = new Node(data);
        } else {
            Node<T> newNode  = new Node(data);
            newNode.previous = this.tail;
            this.tail.next = newNode;
            this.tail = newNode;
        }
    }

    @Override
    public T removeAtHead() {
        if(this.head == null) {
            System.out.println("Stack Underflow");
            return null;
        }
        T data = this.head.data;
        if(this.head.next != null) {
            this.head.next.previous = null;
            this.head = this.head.next;
        }
        if(this.head == null)
            this.tail = null;
        return data;
    }

    @Override
    public T removeAtTail() {
        if(this.tail == null) {
            System.out.println("Stack Underflow");
            return null;
        }
        T data = this.tail.data;
        if(this.tail.previous != null) {
            this.tail.previous.next = null;
            this.tail = this.tail.previous;
        }
        if(this.head == null)
            this.tail = null;
        return data;
    }

    @Override
    public void printFromHead() {
        if(this.head == null) {
            System.out.println("List is Empty");
            return;
        }
        Node<T> itr = this.head;
        while(itr != null) {
            System.out.print(itr + " --> ");
            itr = itr.next;
        }
        System.out.println("\\");
    }

    @Override
    public void printFromTail() {
        if(this.tail == null) {
            System.out.println("List is Empty");
            return;
        }
        Node<T> itr = this.tail;
        while(itr != null) {
            System.out.print(itr + " --> ");
            itr = itr.next;
        }
        System.out.println("\\");
    }
}

class DoublyLinkedListTest {
    public static void main(String... args) {
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        d.insertAtHead("visrut");
        d.insertAtHead("het");
        d.insertAtTail("jay");
        d.printFromHead();
    }
}