public class LongestCommonSubSequence {
    static int longestCommonSubSequence(String str1,String str2) {
        int len1 = str1.length();
        int len2 = str2.length();
        int[][] array = new int[len1+1][len2+1];

        for(int i=0;i<=len1;i++)
            array[i][0] = 0;
        for(int i=0;i<=len2;i++)
            array[0][i] = 0;

        for(int i=1;i<=len1;i++) {
            for(int j=1;j<=len2;j++) {
                if(str1.charAt(i-1) == str2.charAt(j-1)) {
                    array[i][j] = 1 + array[i-1][j-1];
                } else {
                    array[i][j] = Math.max(array[i][j-1],array[i-1][j]);
                }
            }
        }

        return array[len1][len2];
    }

    public static void main(String[] args) {
        System.out.println(longestCommonSubSequence("ABAB","BAB"));
        System.out.println(longestCommonSubSequence("ABB","BAB"));
        System.out.println(longestCommonSubSequence("ABB","X"));
    }
}