#include <iostream>
#include <cassert>

int longest_common_subsequence(std::string str1,std::string str2) {
    int len1 = str1.length();
    int len2 = str2.length();

    int arr[len1+1][len2+1];

    // if length of string instance is 0 then result is also 0
    for(int i=0;i<=len1;i++)
        arr[i][0] = 0;
    for(int i=0;i<=len2;i++)
        arr[0][i] = 0;

    for(int i=1;i<=len1;i++) {
        for(int j=1;j<=len2;j++) {
            // if both char of string match then add 1 into previous result and store
            if(str1[i-1] == str2[j-1])
                arr[i][j] = 1 + arr[i-1][j-1];
            else {
            // if they don't match then take maximum from substring
                arr[i][j] = std::max(arr[i][j-1],arr[i-1][j]);
            }
        }
    }

    return arr[len1][len2];
}

int main() {
    assert(longest_common_subsequence("AB","AB") == 2);
    assert(longest_common_subsequence("ABAB","B") == 1);
    assert(longest_common_subsequence("CBHG","Y") == 0);
    assert(longest_common_subsequence("AB802LI","li") == 0);
    assert(longest_common_subsequence("TUELQCI@*","TRYQTPI@") == 4);
    return 0;
}
