class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None

    # insert at head node is pointing to
    def insertAtHead(self,data):
        newNode = Node(data)
        if self.head == None:
            self.head = newNode
        else:
            newNode.next = self.head
            self.head = newNode

    # remove at head
    def removeAtHead(self):
        if self.head == None:
            print("Underflow")
            return None
        data = self.head.data
        self.head = self.head.next
        return data

    # print linked list
    def printList(self):
        if self.head == None:
            print("List is Empty")
        else:
            itr = self.head
            while itr != None:
                print(itr.data,end=" ")
                itr = itr.next
            print()

l = SinglyLinkedList()
l.insertAtHead("visrut")
l.insertAtHead(1)
l.insertAtHead(1+1j)
l.insertAtHead(1.1)
l.printList()